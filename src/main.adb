with Ada.Text_IO; use Ada.Text_IO;
with Ada.Command_Line; use Ada.Command_Line;
procedure Main is

   can_stop : boolean := false;
   cur_id : integer := 0;
   pragma Atomic(can_stop);
   pragma Atomic(cur_id);

   time : duration := 0.0;
   threads : integer := 0;

   task type Break_thread;
   task type Runner;

   task body Break_thread is
   begin
      delay time;
      can_stop := true;
   end Break_thread;

   task body Runner is
      sum : Long_Long_Integer := 0;
      cnt : Long_Long_Integer := 0;
      p_id : integer := 0;
   begin
      p_id := cur_id;
      loop
         sum := sum + 2;
         cnt := cnt + 1;
         exit when can_stop;
      end loop;
      delay 1.0;

      Put_Line("Потік: [" & p_id'Img & "] зупинено з сумою: " & sum'Img & ", кількістю кроків: " & cnt'Img);
   end Runner;
   
   breaker : access break_thread := null;
   cur_runner : access runner := null;
   i : integer := 1;
begin
   if Argument_Count < 2 then
      Put_Line("Відсутні аргументи програми - кількість потоків та час очікування в секундах");
   else
      time := Duration(Integer'Value(Argument(2)));
      threads := Integer'Value(Argument(1));
      Put_Line("Запускаємо потоки...");
      while i <= threads loop
          cur_id := i;
          cur_runner := new Runner;
          i := i + 1;
      end loop;
      breaker := new Break_Thread;
      Put_Line("Потоки запущено, очікуйте...");
      Put_Line("");
   end if;
end Main;
